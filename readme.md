# Plex Server
> This repo hosts scripts and playbooks associated with setting up a Plex media server and retrieving files from remote servers/sources to populate the library of the plex media server.
## There are 2 main playbooks for this repo at this time:
> Additional playbooks objectives would be to gather media from other sources.

### playbooks/plex_server.yml
---
- This playbook is used to setup a plex server on the localhost machine, with directories mounted for movies, tv and config
	```
	server_directory/
	├── config
	├── movies
	└── tv
	```

###  playbooks/download_file.yml
---
- This playbook downloads a specified file from a remote server
- Run playbook using command as follows
	```
	ansible-playbook playbooks/download_file.yml --vault-password-file secret_password_file
	```
	> Download custom files
	```
	ansible-playbook playbooks/download_file.yml --vault-password-file secret_password_file --extra-vars="file_path=/path/to/videofile/video.mp4 media_type=movies"
	```
- Variables to consider
	- `file_path`: this dictates what file is downloaded
	- `media_type`: `movies` or `tv` are valid choices, this will download the file into movies or tv directory accordingly

- `secret_password_file` : is a secret file that exists only on the lab server to encrypt the ssh_key



## Python Runner
---
> python_runner.py simply runs the download_file.yml playbook by intaking 2 arguments (file_path, and media_type)
- install requirements
	```
	pip3 install -r requirements.txt
	```
- run command
	```
	python3 python_runner.py <file_path> --media-type <media_type>
	
	ex : (only `movies` or `tv` are valid media types)

	python3 python_runner.py banana.mp4 --media-type movies
	```
	> This is equivalent to `ansible-playbook playbooks/download_file.yml --extra-vars="file_path=banana.mp4 media_type=movies"`<br>
	> Result : `banana.mp4` is downloaded from the remote server into `<plex_server_media_location>/movies`

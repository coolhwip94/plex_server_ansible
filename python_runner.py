import ansible_runner
import sys
import argparse


def call_ansible_download_playbook(file_path, media_type="tv"):
    '''
    Simple function to run static ansible playbook to download file from remote server for plex.

    Params:
    file_path: the path of the file on the remote server
    media_type: "tv" or "movies" are valid choices, "tv" is default

    '''

    out, err, rc = ansible_runner.run_command(
        executable_cmd='ansible-playbook',
        cmdline_args=['playbooks/download_file.yml',
                      f'--extra-vars="file_path={file_path} media_type={media_type}"'],
        input_fd=sys.stdin,
        output_fd=sys.stdout,
        error_fd=sys.stderr,
    )


if __name__ == '__main__':

    # Gather arguments
    # file_path and media_type
    parser = argparse.ArgumentParser(description='Download file from remote server using ansible-runner')

    parser.add_argument('file_path', type=str,
                        help='file_path on the remote server to download')
    parser.add_argument('--media-type', type=str, help='"movies" or "tv" default is "tv"')

    args = parser.parse_args()

    # Use arguments to call ansible playbook
    call_ansible_download_playbook(file_path=args.file_path, media_type=args.media_type if args.media_type else "tv")
